# Department registration
1. [Submitting a job offer](https://intranet.donders.ru.nl/index.php?id=5448)   
2. [Student registration](https://intranet.donders.ru.nl/uploads/media/24112019_Application_form_contract_HR_ENG_01.docx)


# Animal experiment registration   
1. [General explaination](https://www.radboudumc.nl/en/research/radboud-technology-centers/animal-research-facility/animal-experiment#69247)   
2. [Student intern](https://www.radboudumc.nl/getmedia/5ea8d34b-32de-480d-a9cd-481f3410cc2b/Registration_student_trainee_Nov2020.docx.aspx)
3. [Art. 9 registration](https://www.radboudumc.nl/getmedia/2c227d68-8aa6-4c6d-aee9-8c0f27e31484/Registration_article-9_Jan2020_1.docx.aspx)

# Project folder





