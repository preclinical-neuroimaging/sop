SOP for animal habituation

1. [Preparation](#preparation)
2. [Consumables](#consumables)
3. [Method](#method)
4. [Follow-up](#follow-up)
5. [References](#references)


### Preparation
what tools, where, what to book in advance

1. Mouse Cradle 
2. Imitation MRI bore (Tube)

### Consumables
what consumables, where to find it, how much to use

1. Mouse food (sunflower seeds)

### Method

#### Habituation to experimenter's hands and body tube

30 minutes before the start of each habituation (to head-fixation and fMRI setup) procedure, mice will need to acclimate to the experimenter's hands and body tube. This procedure lasts for about 30 minutes. 

1. Place 2 sunflower seeds in the mouse's cage for 10-15 minutes while removing any objects that the mouse can hide in (tubes, running wheel, cotton nets, etc.). 
2. Slowly, corner the mouse with the hand until it eventually climbs up. Alternatively, put 2 sunflower seeds on the hand to motivate the mouse to climb up to the hand. The mouse is held in hands for about 5 to 10 minutes until it calms down. A calmed mouse can be seen by its grooming behavior. A water reward of 0.2 ml will be administered to the mouse using a syringe as a reward. 
3. Next, introduce the body tube to the mouse to let the mouse explore until it enters the tube. This procedure is repeated 4 to 5 times until the mouse deliberately enters the tube.

#### Habituation to head-fixation and fMRI setup

Mice will undergo up to 7 days of habituation training to being head-fixed into an MRI setting with incremental training sessions. The habituation will take place outside the MRI system using a setting that replicates the interior of the magnet bore. Furthermore, to mimic the environment inside the magnet bore during imaging, habituation will be conducted inside a ventilated acoustic chamber where a tape recording of the noise generated by imaging gradients will be played. Animal body restraint may be assisted with a cloth wrapped around the animal's body to restrict limb motion during habituation and imaging.

* Day 1: Mice are introduced to the cradle and given 5 minutes to explore it (or if cloth wrap is necesarry, will have a day with only the wrap restriction).
* Day 2: Mice are head-fixed into an MRI setting for 10 minutes.
* Day 3: Mice are head-fixed into an MRI setting for 10 minutes accompanied with MRI sounds.
* Day 4: Mice are head-fixed into an MRI setting for 30 minutes.
* Day 5: Mice are head-fixed into an MRI setting for 30 minutes accompanied with MRI sounds.
* Day 6: Mice are head-fixed into an MRI setting for 60 minutes.
* Day 7: Mice are head-fixed into an MRI setting for 60 minutes accompanied with MRI sounds.

The effectiveness of this habituation procedure in minimizing the stress experienced by the animal will be assessed by alterations in physiological parameters including body motion, respiratory rate, and plasma corticosterone and cytokines (see section 8 for blood collection). Blood collection will be performed twice per animal, once prior to habituation for baseline and once after the first fMRI scan, to measure the difference in stress hormones and thus to assess animal stress levels. Once the suitable habituation protocol has been devised, no further blood collection will be required. In instances when only corticosterone is required, e.g. no changes in cytokines observed in previous experiments, faeces will be collected instead of blood samples. Animals that fail to train to the paradigm will be excluded from the awake imaging protocol.

#### Head-fixation and body restrain

Head-fixation is required to minimize the movement of mice during the habituation and imaging (2)

1. Anesthetize the mouse with 5% isoflurane in 1:1 air to oxygen mix in a designated induction box. Once the mouse is anaesthetized, the head-fixation procedure can begin. 
2. Position the wings of the head bar into notches in a stainless-steel holder and fixed with a pair of clamps and thumbscrews. 
3. Then, insert the mouse body into the acrylic 'body tube' in such a position that the mouse head extends out while their front paws gripping the tube edge or ledge after head-fixation. 
4. Attach the holder and body tube to the caddy. The head bar is approximately 30 mm above the bottom of the body tube. 
5. After the caddy is attached, the fix the caddy to the behavior box using magnetic kinematic bases. The mounts will facilitate the experimenter to head-fix mice outside the apparatus in the caddy. 
6. Put the mouse attached to a caddy into the apparatus rapidly and consistently. A head-fixed mouse should crouch in a natural position in the body tube, with paws resting on a tube edge or a ledge.

![head-fixed](assets/img/head-fixed_design.png)



**Figure 1** | Animal head-fix implant design and schematic of the animal (mouse) cradle with head-fix holding system, an optical implant for optogenetics, anaesthesia/odour delivery cone, and lick detector.


#### Blood collection

Blood withdrawal will be made using a razor blade to make a small puncture on a tail vein on the last quarter of the mouse tail in animals held in a plastic restrainer. Up to 50 ul shall be taken per collection, for up to 3 repeated times each spaced 2 days apart, for a total of 150 ul. The lesion site will be pre-washed with ethanol to prevent infection. Blood collection will be performed twice per animal, once prior to habituation for baseline and once after the first MRI scan, to measure the difference in stress hormones and thus to assess animal stress levels. Once the suitable habituation protocol has been devised, no further blood collection will be required.

The step-by-step protocol will be written here.


### Quality assurence

### Follow-up
What do to in the following days

### References

2.	Guo, Z. V., Hires, S. A., Li, N., O'Connor, D. H., Komiyama, T., Ophir, E., . . . Svoboda, K. (2014). Procedures for Behavioral Experiments in Head-Fixed Mice. PLOS ONE, 9(2), e88678. doi:10.1371/journal.pone.0088678