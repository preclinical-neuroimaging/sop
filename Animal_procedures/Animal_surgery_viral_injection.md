SOP for animal surgery

1. [Preparation](#preparation)
2. [Consumables](#consumables)
3. [Method](#method) 
4. [Follow-up](#follow-up)


### Preparation

Abbreviations for where to find the material/consumables:
* Present in surgery room = PSR
* Present in animal prep room = PAR
* Ask Jo = AJ (or Roël)
* Ask Bianca = AB (or another PRIME worker)
* Ask Bas (van Gorp) = (ABvG)

IMPORTANT: First book the surgery room at PRIME for the surgery date! Make sure it is a DM-1 room (eg. two-photon room)
ALSO IMORTANT: booking the surgery room does not mean that the heating chambers/heat pads are there. Talk with the CDL staff if those are also available (and ask where to find it)

#### Material
1. Scissors (AJ)
2. Scalpel (AJ)
3. Tweezers (AJ)
4. Dental drill (in two-photon room)
5. MRI-compatible Plexiglas or 3D-printed plastic head implant (AJ)
6. Scale (AJ or AB)
7. Shaving apparatus (PSR or PAR)
8. Temperature measurement apparatus (rectal) (PSR)
9. Heated Recovery chambers (for post surgery recovery) (PSR, note: can vary)
10. Heat-pad for surgery (PSR, note: can vary)
11. Isoflurane delivery apparatus (including the induction chamber and extension part for delivery to the snout during surgery) (PSR)
12. Air extraction system (to remove air contaminated with isoflurane from the operation table) (PSR)
13. Sand paper to scratch the skull (Ask Andor if not in Jo's cabinet)
14. Ear clipping tool (to mark the animal)
15. Stereotact (ABvG)
16. Syringe Holder / Pump (ABvG)
17. Ice to keep the virus on (can be found on the main floor, go through the door behind the lunch room, from there, first door to the left)


### Consumables

what consumables, where to find it, how much to use

1. Betadine (PSR)
2. Injection needles (for analgesia preparation (10mL) and injection(1mL)) (PSR)
3. Glass pipette (PSR)
4. Saline 2x (PSR)
5. Diaper 2x (PSR)
6. Cotton swabs (PSR or AB)
7. Tissues (PSR)
8. Dental cement (Sun Medical) (AJ)
10. Carprofen 10 mg/kg (PAR)
11. Anaesthesia (isoflurane 4-5%) (PSR)
12. Analgesia: (AB)
    * Lidocaïne HCL 10mg/ml
    * Bupivacaïne Actavis 5 mg/ml
13. Depilatory cream, feet cream (AB)
14. Eye gel (PSR)
15. Virus aliquots (AJ) 

IMPORTANT: For the virus, aliquots of 5 uL should be available in the -80. Do check this beforehand. Also, it is bad to thaw and refreeze the virus, so try to prevent this as much as possible. 

### Method

#### Days before the surgery
* D-3, 4, and 5 days: Weigh the mouse to ensure that the dosage of anaesthesia and analgesia is be given correctly.
* D-2 days: Add the running wheel to the cage (optional).
* D-1 day: Add the carprofen to the water to establish an effective plasma level. Dose is 0,1 per 100 ml drinking water. This will stay present until post-surgery day 2 (or if the recovery stagnates).

#### Day of the surgery
#### Instrument preparation
* The following instruments are collected:
    * Ethanol 70%
    * Betadine
    * Needles (1 x 10mL needle for analgesia prep. 1 x (per animal!) 1mL needle for subcutane injection) 
    * Isoflurane 
    * Glass pipette
    * Saline 2x
    * Cura-Heat Heat pad 
    * Diaper 2x
    * Cotton swabs
    * Hair removal cream (Brandname = "Veet")
    * Tissues
    * Dental cement
    * Eye gel
    * Analgesia: Lidocaïne HCL 10 mg/ml and Bupivacaïnve Actavis 5 mg/ml
    * Sand paper  

* The following instruments are prepared AND cleaned with H2O2:
    * Surgery equipment:
        * Scissors
        * Scalpel
        * Tweezers
    * Dental drill
    * MRI-compatible Plexiglas or 3D-printed plastic head implant
    * Shaving machine
    * Scale  

* For the dental cement, put the mixing stone in the freezer (the cold will prevent the cement from hardening too quickly)

#### Room/animal preparation

* * Check if standard equipment is present.
    * Lab coat, gloves, head covers, shoe covers, and surgical mask.  
* Transfer the mice from the animal prep room to the surgery room
* Prepare analgesia:
    * 1 mL of lidocaïne + 2 mL Bupivacaïne + 8,5 ml Nacl: total volume = 11,5 mL. 
    * Dosage is 0,2 ml per 100 gr mouse
    * Determine the right dosage based on the weight of the mouse, prepare the injection needle
* Make sure the heat recovery rooms are turned on
* Make sure the heating pads/temparature measurement systems are on
* Weigh mouse (write this down!)

* Two tables will be used and cleaned with H2O2 prior to the surgery.
    Table 1 (preparation) is for anesthesia induction, clipping of hair and cleaning:
    * Diaper on table
    * Shaving machine
    * Hair removal cream
    * Cotton swabs
    * Ethanol for removal of the cream
    * Tissues
    * Eye gel  
     
    Table 2 is for all surgical procedures:
    * Diaper on table
    * Betadine
    * Ethanol 70%
    * Surgery equipment: scissors, scalpel, tweezers
    * Make sure the air extraction system is placed on top of the table, so that isoflurane is not entering the room
    * Enter the head pat under the diaper (make sure it is on)
    * Set temparture measurement apparatus ready (make sure it is on)
    * Dental drill 
    * Eye gel
    * Dental cement (does not have to be prepared yet. Wait with preparation right before it is going to be used)
    * Stereotact installed with the syringe holder and pump. 

*Put the syringe (with the correct needle) in the syringe holder and rinse it 3x with both 70% Ethanol and Distilled water. Then fill it with the virus solution (only the needed amount plus a bit extra for safety)  


### Surgery 

1. Make sure that all surgery tools are autoclaved prior to each session. The tools need to be thoroughly cleaned with ethanol between each animal and sterilized using hot microbeads. All efforts are made to work in aseptic conditions. 
2. Turn on Isoflurane and Oxygen to fill induction chamber (5 min).
3. Place the mouse in a chamber and monitor its locomotion and breathing.
4. Once sufficiently sedated, transport the mouse to the heat pad on table 1.
5. Make sure that the isoflurane is now being deliverd to the snout of the mice instead of the induction chamber by adjusting the three-way tube. 
6. Hold the mouse in one hand and use the shaving machine to dispose of the hair present around the site of injection/operation (if you do this quicly, you do not need to deliver the isoflurane to the mouse right away)
7. Put the mouse on the diaper and make sure that the isoflurane is reaching the snout (mouse keeps being sedated).
8. Put eye gel on the eyes of the mouse. 
9. Put the hair removal cream over the spot that is previously shaved. For this use cotton swabs. Wait for 1/2 min. Removel the hair and the cream with ethanol (thorougly!).
10. Make sure that the skin is cleaned with ethanol.
11. Inject the analgesia (previously prepared) subcutaneously.
12. Transfer the mouse to the recovery chamber. If it has woken up properly, the mouse can be transferred back to its cage. 
13. Wait at least 30 min, before continuing with the surgery. It takes 30 min for the analgesia to work! Thus, set a timer for this.  

14. After 30 min, place the mouse back into the induction chamber. Monitor its locomotion and breathing.
15. After the mouse is fully anaesthetized, move the mouse to the stereotact on table 2 and restrain its head. 
16. Make sure that the isoflurane is now being deliverd to the snout of the mice instead of the induction chamber by adjusting the three-way tube.
17. Measure the body temparature via placing the meter rectally (monitor the body temperature). 
18. Apply eye gel and re-apply it regularly throughout the surgery to prevent the drying of the eyes.
19. Clean the exposed skin of the mouse with Betadine, followed by ethanol. Repeat this 3 times!
20. Assess its pedal reflex by pinching its toe or paw. If the mouse is fully sedated, it would not react to the toe or paw pinch. 
21. From this point onwards, monitor regularly the breathing rate of the mouse.  

22. Cut away the flap of skin head using tweezers and a sharp scissor. First, start with a small circle in the middle of the skull until sagittal suture (a dense, fibrous connective tissue joint between the two parietal bones of the skull) is visible. Using this as an orientation point, create a large enough circular opening so the head plate will fit in. Make sure that the remaining membranes near the edges are cut away to provide a direct connection of dental cement to the skull. 
23. Scratch the skull with the sandpaper, consequently use the metal drill to make more scratches on the skull.
24. Clean the exposed skull with saline and cotton swab when needed.  

25. Calibrate the syringe holder by locating Bregma on the skull
26. Once calibrated, place the needle over the injection location using the coordinate system (for 2019-0023-002: ML: 1.5/-1.5 mm, AP: 0.5 mm and DV: 2.5 mm)
27. Move the needle up and with the dental drill CAREFULLY drill a hole in the skull (the skull is thin, so you won't need to much force, be carefull not to damage the brain underneith) 
28. Clean any debris around the hole with saline
29. SLOWLY move the needle down to the required coordinates and inject the virus at rate of 100 nl/min (for 2019-0023-002: hM4D(Gi) = 300nl, Control = 255nl)
30. Wait for 5 minutes after the injection has finished to make sure the bolus can spread and wont be pulled up with the needle again
31. SLOWLY move the needle up again 
32. Clean the needle tip and check if it's not clogged
33. Repeat on contralateral side  
 
34. Prepare the dental cement by combining the powder, liquid substance and activator. This preferably can be done by a second person. Be aware for the thickness of the cement. 
35. Attach the custom-made MRI-compatible Plexiglas or 3D-printed plastic head implant (head plate) to the skull. 
36. Cover the head plate and exposed skull with more dental cement. This is done by applying dental cement to the underside of the head plate and positioning it on the skull. Make sure that the head plate and the skull is completely connected with dental cement. 
37. Transfer the mouse to the recovery chamber. Monitor the mouse and let the dental cement dry. 
38. After the mouse is sufficiently recoverd, transfer the mouse back to its cage.   


### Quality assurence
It is of great importance that the headplate is connected to the skull, so that it remains connected throughout all procedures. Therefore, check every day if the headplate is still on its place (especially for ethical reasons). After the surgery, let the mouse recover for 7 days. Before initiating the habituation procedure, "wiggle" the headplate and check if it is attached properly. 

### Follow-up
1. Weigh the mouse on D-1 and D-2 post-surgery. Monitor its weight loss. If at D-2 the mouse keeps losing weight (compared to weight before and after surgery), place wet food in the corner of the cage. If the mouse has a greater weight loss than 15%, notify the animal caretaker.
2. On post-surgery D-2, remove the carprofen (10 mg/kg) from the drinking water, if the mouse has sufficiently recovered from the surgery (shows clean fur, normal body posture, steady body weight, grooming/active behaviour).
3. Monitor the mice daily for signs of sickness/if the headplate is still in its place.
4. Administrate systemic antibiotics (ampicillin, 5-25 mg/kg) i.p. if animals show signs of infection. 
5. The total surgery recovery duration is 7 days. After this, the habituation protocol can be initiated. 