SOP for scanning mice

1. [Preparation](#preparation)
2. [Scanning protocol](#protocol)
3. [Cleanup](#cleanup)

### Preparation
Setup the coils, put in the cradle, treadmill etc

### Protocol

Create new study/subject (depending on if its a new mouse or just a new session)

When performing fMRI combined with chemogenetics, inject the mouse with 0.1 mL of either CNO or vehicle prior to the EPI's. Keep in mind that CNO takes 30min to work, so inject far enough in advance of the EPI, e.g. before the EPI's of the previous mouse. 

1. Localizer
2. Localizer Multi-slice
	- If first mouse, then wobble (under adjustments)
3. Anatomical (TurboRARE)
4. fMRI single
	- Copy parameters from anatomical
	- B0 Map (under adjustments)
5. fMRI multi (repeat as many times as wanted)
	- Copy parameters from fMRI single
	- If using camera, make sure to start code before start of scan 

### Cleanup
Take out coils/cradle, if necessary, clean the treadmill