SOP for animal training

1. [Preparation](#preparation)
2. [Consumables](#consumables)
3. [Method](#method)
4. [Follow-up](#follow-up)


### Preparation
1.  Check wheter the water adjustments have been continued before the start of training (and previously for habituation) by adding critic acid into the drinking water of the mice.
2.  Habituation (x days) has to be completed. The x day of habituation entails lick teaching, and thus on this day animal training is initiated.
3.  Check if the software needed for training is ready (lick teaching, odor conditioning, odor discrimination or full-task learning).
    * Go to file manager. The training programs can be found at the path: Student/Documents/Training
4.  Check if head-fixing system, training tube and the apparatus containing the lick-detector, odor and reward delivery system is available.
5.  Check if a person with article 9: animal handling certificate is available during the training days. 
6.  Make sure that a room at the animal facility centre suitable for animal training is availble during the training days (book this in advance).
7.  .....


### Consumables
what consumables, where to find it, how much to use
1.  Syringe that can be used to deliver water to mouse (for 'Animal set-up')
2.  Gloves for animal handling
3.  .....

### Method
Animal training will be conducted in seven days and follows after the habituation procedure. First, the steps for the animal set-up are described. 
Consequently, the following training phases are elaborated: lick teaching, odor conditioning, odor discrimination and full-task learning. After following the steps below, the mouse is able to perform the Go/No-Go task with a sufficient performance.

**Animal set-up**

The following procedure has to be performed at the beginning of each training (with each training phase).
1.  Corner the mouse and let the mouse climb onto the experimenter's hands.
2.  Let the mouse calm itself down, while being held until grooming behavior is observed or when the mouse is drinking when water is being offered.
3.  Let the mouse explore the training tube until it climbs in. Give the mouse a water reward as it has entered the tube.
4.  Rapidly head-fix the mouse. Give the mouse a water reward, while it is head-fixed.
5.  Set the apparatus (consisting of the lick-detector, odor and reward delivery system) at the appropriate distance to the mouth of the mouse so that it can smell and reach the water with its tongue: 0,5 mm below the lower lip and 5 mm posterior to the tip of the nose.
6.  The mouse is now prepared, where training (lick teaching, odor conditioning, odor discrimination or full-task learning) can be initiated.

**Lick teaching**

Lick teaching is perfomed after the last day of habituation (this is considered day 1 of animal training).
1. Follow the steps at 'Animal Set-Up'.
2.  Prepare the software for lick teaching. Name of the program is: Step 1 lick teaching. 
3.  Open the Arduino program and upload it to the Arduino Board and click immediately on the icon "Serial Monitor" (upper right corner). The training program starts. On the serial monitor you can follow the programs progress and the behavior of the mouse.
4.  The first part of lick teaching consists of a sequence of reward delivery that is indepedent of licking (see Figure 1. Lick teaching).
     - Amount of water per reward: 5 µl
     - Number of repeats: 20
     - On the serial monitor you can see this as: "Start loop" following the count (how many times a reward is given).
5.  After the delivery of 20 rewards, the program switches to the lick-reward trial (see Figure 1. Lick teaching). A reward is delivered for every lick.
     - Amount of water per reward: 5 µl
     - Number of repeats: 5 trials of 20 hits (received rewards)
 
- Between each completed trial, move the water port away for 1 minute and place back at the original place. In this way the mouse can make the association between the presence of the water port with a reward and therefore reinstates its licking behavior.
- If no licking takes place for one minute, the water port is manually moved closer to the mouth (so that the droplet can touch the fur) and one water reward is given. This always causes the animal to lick (Guo et al. 2014). This is done by pressing on the button "Start" on the syringe pump. Consequently, the water port is placed back to the original position and lick teaching resumes.
- **WARNING** WITH STARTING THE PROGRAM AGAIN AFTER ONE TRIAL: uploading the program again onto the Arduino Board again will also initiate the first part of lick teaching (however this only has to be executed once). Therefore, in the program, set the const int NUMBER_OF_REPEAT to 0 after the first trial is completed. By changing this variable, the program does not execute this part but goes straight to the lick-reward trial. 
- The amount of water that is rewarded varies per mouse: minimum of 720 µl plus the rewards for no-lick cases (5 µl each).
 
6.  After a total of 100 rewards the training is ended. 
7.  The mouse can be released from the head-fix and put back into its cage. Training will continue the next day.

!*it has to be determined how many mice can be done on one day and how much time it takes for one mouse to complete the training* 

 
![Lick teaching](assets/img/Lick_teaching.jpg)

Figure 1. Lick teaching



**Odor conditioning**

Odor conditioning is performed on the second and thirth day of animal training.
1.  Follow the steps at 'Animal Set-Up'.
2.  Prepare the software for odor conditioning learning. Name of the program is: Step 2a Odor conditioning. Upload the program. The program starts. 
3.  The first part of odor conditioning consists of a sequence of odor 1 presentation, followed by a reward delivery that is independent of licking (see Figure 2. Odor conditioning).This will provoke the licking behavior of mice (Nixon et al. 2012).
    - Amount of water per reward: 5 µl
    - Number of repeats: 40
    - Odor delivery duration: 2 seconds

4.  After completion of Step 2a, continue with the odor conditioning trial (see Figure 2. Odor conditioning). The name of the program is: Step 2b Odor conditioning.
5.  Open the Arduino program and upload it to the Arduino Board and click immediately on the icon "Serial Monitor" (upper right corner). The training program starts. On the serial monitor you can follow the programs progress and the behavior of the mouse.
    - At the Serial Monitor you can read the amount of "Hits" (licking during odor 1 presentation) and "Licks" (total amount of licks). 
6.  A reward is delivered when the mouse licks in the response window after odor 1 delivery (also considered "Hits").
    - Amount of water per reward: 5 µl
    - Number of repeats: x trials of 20 odor presentations
    - Odor delivery duration: 2 seconds

- A trial consists of 20 odor presentations. 
- After the completion of FIRST trial, you have to determine the baseline measurement. This is the amount of hits relative to the amount of odor 1 presentations that are provided. In case of 20 odor presentations, the hit rate is 50% when the mouse has received 10 rewards (and thus has licked in 10 of the 20 odor presentations).
- After the determination of the baseline hit rate, Start a new trial (see Figure 2).
- In the next trials, observe if there is an improvement in the hit rate. If the hit rate is above the baseline or the pervious measurement, start a new trial.
- When there is no improvement in hit rate compared to previous trials, repeat Step 2a of odor conditioning but only for 5 repeats instead of 40. For this, change the const int NUMBER OF REPEAT to zero (instead of 40) in the software of Step 2a. Consequently start a new trial.
- The amount of water that is rewarded varies per mouse: minimum of 740 µl plus the rewards when Step 2a is repeated. 

7.  After a total of 3 trials (where there is a consecutive improvement in the hit rate) the training is ended.
8.  The mouse can be released from the head-fix and put back into its cage. Training will continue the next day.

* NOTE: As you can see in Figure 2, the exact amount of odor presentations, repeats and trials to be completed are arbitrary (also illustrated as "X repeats/presentations"). The repetitions mentioned above are a first direction. It has to be tested on the mice which amount would be sufficient. 
![Odor conditioning](assets/img/Odor_conditioning.png)

Figure 2. Odor conditioning


**Odor discrimination**

Odor discrimination is performed on the fourth and fifth day of animal training. 
1. Follow the steps at 'Animal Set-Up'.
2. Prepare the software for odor discrimination learning. Name of the program is: Step 3a Odor discrimination (also open, but not upload, Step 3b and step 3c). Upload the program. The program starts. 
3.  The first part of odor discrimination consists of a sequence of odor 2 and odor 1 presentation, followed by a reward delivery that is independent of licking (see Figure 2. Odor discrimination). With this a second odor is introduced (non-rewarding odor).
    - Amount of water per reward: 5 µl
    - Number of repeats: 20
    - Odor delivery duration: 2 seconds

4. After completion of step 3a, continue with step 3b (for this upload this program in Arduino). Now only licking during odor 1 delivery will result in a reward (see Figure 3. Odor discrimination).
    - Amount of water per reward: 5 µl
    - Number of repeats: x trials of 20 odor presentations (10 of odor 1 and 10 of odor 2)
    - Odor delivery duration: 2 seconds

- Odor 1 is followed by odor 2, in which the mouse has to lick during odor 1 presentation, which continues until a total of 20 odors are deliverd. At 20 deliveries, one trial is completed.
- After the completion of FIRST trial, you have to determine the baseline measurement. This is the amount of hits relative to the amount of odors presentations that are provided. In case of 20 odor presentations (10 odor 1 and 10 odor 2), the hit rate is 50% when the mouse has received 5 rewards (and thus has licked in 5 of the 10 odor 1 presentations).
- After the determination of the baseline hit rate, give the mouse a sequence of only odor 2 delivery (5 repeats). For this, upload program named: Step 3c Odor discrimination. Here it is aimed that when the mouse is continiously licking, it perhaps better learns to associate only odor 1 with a treat. Via the "Serial Monitor" you can follow the licking behavior of the mouse. Keep track of how many times the mouse keeps licking during this sequence. 
- Start a new trial (see Figure 2).
- In the next trials, observe if there is an improvement in the hit rate. If the hit rate is above the baseline or the pervious measurement, give again a sequence of odor 2 (5 repeats). Check whether the amount of licks have decreased during odor 2 presentation (Step 3c). This will give an extra assurance that the mouse knows the difference between odor 1 and 2.

- When there is no improvement in hit rate compared to previous trials, repeat Step 3a of odor conditioning but only for 5 repeats instead of 20. For this, change the const int NUMBER OF REPEAT to zero (instead of 20) in the software of Step 3a. Consequently start a new trial.
- The amount of water that is rewarded varies per mouse: minimum of 740 µl plus the rewards when Step 2a is repeated. 


5.  After a total of 3 trials (where there is a consecutive improvement in the hit rate) the training is ended.
6.  The mouse can be released from the head-fix and put back into its cage. Training will continue the next day.

![Odor discrimination](assets/img/Odor_discrimination.png)

Figure 3. Odor discrimination


**Full task learning**

Full task learning is performed on the sixt and seventh day of animal training. For each day, the task is repeated 5 times per mouse to be able to determine performance rates. In the software, one loop (standard setting via GUI) contains two odor presentations (randomly odor 1 or odor 2), where the number of repeats in the loop is set on 5. This results in 10 odor presentations in one run. Set this parameter to 10. The total odor presentations of one run (encompassing 20 odor presentations) will be used to calculate performance rates.
1.  Follow the steps at 'Animal Set-Up'.
2.  Prepare the software for the Go/No-go task. The instruction on how to prepare/use this software to perform the task can be found in Gitlab. Go to: lab_book, Femke's Branch, Go_no-go_task. File name is "Set-up software_hardware". 
3.  This phase entails the full learning of the Go/No-Go task that later is applied during awake-behaving mouse fMRI.
    Licking behavior is defined as follows:
    - Odor 1 delivery with detected lick    = HIT
    - Odor 1 delivery without detected lick = MISS
    - Odor 2 delivery with detected lick    = FALSE CHOICE (FC)
    - Odor 2 delivery without detected lick = CORRECT REJECTION (CR)
4.  Per 20 odor presentations (2 x 5 loop repeats with two odor presentations), the performance rates have to be calculated to determine to what extent the mouse has learned the Go/No-Go task.
    The performance of the correct rate, hit and CR rates (%) are calculated as follows:
    - The performance of the correct rate:
      Number of HITs + Number of CR  /  (Total number of trials (20))
    - Hit rate:
      Number of HITs  /  (Number of HITS + Number of MISSes)
    - CR rate:
      CR  /  (CR + FC)
5.  After the completion of 100 trials (with 5 determined performance correct, hit and CR rates on a day containing 20 trials each) the training is ended.
6.  The mouse can be released from the head-fix and put back into its cage. 
7.  In case of the completion of the last day of full task learning, the points under the header 'Quality assurance' can be followed.


### Quality assurence
After the completion of the animal training, it has to be assured that the mice have sufficiently well learned the Go/No-go task, so that awake-behaving mouse fMRI can be conducted. After the completion of full task learning, you have data on:
*  Two days of full task learning (sixt and seventh day of animal training)
*  Per day, for each mouse, you have 5 performance correct, hit and CR rates that are based on 20 odor presentations

With this information, a graph can be made to illustrate the mean percentage and standard deviations. With this, it can be illustrated how well the mice have learned the task. In Figure 4. Performance rates, an example is shown.

!*also the exact steps to generate the graph can be shown (steps in R that are followed)*
!*here an example for the graph (like in Han et al.) have to be shown when we ourselves have performed the experiments*

Consequently, the following quality standard has to be met in order to label the mice as 'well trained'. Only when this is applicable, the mice are ready to undergo awake-behaving mouse fMRI.
**'Well trained'** is defined as three continuous performance correct rates larger than 80%, each based on 20 odor trials (see Figure 4. Performance rates).

When these criteria are not met after two days of full task learning, the next steps have to be followed:
1.  Extend full task learning with one day.
2.  Follow the steps that are descripted under **Full task learning**.
3.  Check if the quality standard has been met after this extra day of full task learning.
    - In case of yes: training is completed and awake-behaving mouse fMRI can be performed.
    - In case of no: extend full task learning with another day and follow the steps above, until the quality standard has been met.


### Follow-up
When the mice are 'Well trained', the awake-behaving mouse fMRI can be performed. 