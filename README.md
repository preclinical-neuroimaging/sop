# SOP

Lab **S**tandard **O**perating **P**rocedures

### Content
1. General
    1. Code of conduct (to be filled in)   
    2. [Student registration](Misc/New_student.md)   

2. Animal procedures  
    1. Animal surgery procedures
        1. [Headplate surgery](Animal_procedures/Animal_surgery_headplate.md)
        2. [Viral injection](Animal_procedures/Animal_surgery_viral_injection.md)
    2. [Animal habituation procedure](Animal_procedures/Animal_habituation.md)
    3. [Animal training procedure](Animal_procedures/Animal_training.md)
    4. [Animal scanning procedure](Animal_procedures/Animal_scanning.md)
    5. Animal perfusion
        1. [Protocol for Pump in PRIME](Animal_procedures/Perfusion_Mice_Vivienne_PumpPRIME.pdf)
        2. [Protocol TNU](Animal_procedures/TNU-PRO-General-Perfusion_Mouse.pdf)

3. Biochemistry
    1. [ELISA corticosterone measurements](Biochemistry/ELISA_corticosterone.md)
    2. [Viral vector information](Biochemistry/Viral_Vector_information.md)
    3. [CNO solution recipe](Biochemistry/CNO_WS_recipe.md)

4. Code
    1. Arduino code for AMB Training (see AMB GitLab project folder for the full task code)