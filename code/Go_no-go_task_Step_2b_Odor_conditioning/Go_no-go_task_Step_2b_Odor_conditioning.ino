// make loopstates

enum LoopState {
  FIRST_LIGHT,
  SECOND_LIGHT
};
LoopState loopState = FIRST_LIGHT;

long StartTime;
long CurrentTime;
int Timer; 

const int LED1 = 2;
const int LED2 = 3;
const int VALVE3 = 4;
const int LED3 = 6; // green LED, output 3
const int LED_nothing = 13; //LED that has no effect other than bypassing the program
const int BUTTON_2 = 7; // lick detection
const int NUMBER_OF_REPEAT = 20; // amount of water rewards given

int loopCount = 1; // loop iteration
bool button2WasPressed = false; // when lick is detected
bool button2WasPressedP = false;
bool X = false; // to ensure that after 20 lick rewards are given, the program continues with lick-teaching
bool Y;
bool T = false;
bool T2 = false;
bool Z = false;
bool Z2 = false;
bool REWARD;
bool Odor2;
int i;
int Hit = 0;
int Lick = 0;

void setup() {
    Serial.begin(9600); // to see values from Arduino and present it on the computer                              
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    pinMode(VALVE3, OUTPUT);
    pinMode(LED3, OUTPUT); // set LED3 (green) as an output 3
    pinMode(BUTTON_2, INPUT); // set button 2 as an input 2
    pinMode(13, OUTPUT);
}

void loop () 
{
  while(loopCount <= NUMBER_OF_REPEAT)
    {
      reward(i);
      Serial.print("Hits: ");
      Serial.print(Hit);
      Serial.print("  Licks:  ");
      Serial.println(Lick);
    }
  digitalWrite(LED2, LOW);
  digitalWrite(LED1, LOW);
  digitalWrite(VALVE3, HIGH);
}



void reward(int i) {
  CurrentTime = millis();
  
  if (loopState == FIRST_LIGHT) 
    {
      if(T==false)
        {
          StartTime = CurrentTime;
          T = true; 
        }
      Timer = CurrentTime - StartTime;
      if(Timer <= 2000 && T == true) 
        {
          digitalWrite(LED1, HIGH);
          digitalWrite(VALVE3, LOW); 
        }
      else
        {
          digitalWrite(LED1, LOW);
          digitalWrite(VALVE3, HIGH);
          T = false;
          Z2 = false;
          loopState = SECOND_LIGHT;
        }
    }

  if (loopState == SECOND_LIGHT) 
    {
      if(T==false)
        {
          StartTime = CurrentTime;         
          T = true; 
        }
      Timer = CurrentTime - StartTime;
      if (Timer <= 4000 && T == true) 
        {
          digitalWrite(LED_nothing, HIGH); 
        }
      else 
        {
          digitalWrite(LED_nothing, LOW);
          T = false;
          Z = false;
          loopState = FIRST_LIGHT;
          loopCount++;
        }
    }

  button2WasPressed = digitalRead(BUTTON_2);
  REWARD = digitalRead(LED1);
   if (button2WasPressed == true && REWARD == true && Z == false) {
      digitalWrite(LED3, HIGH);
      delay(100);
      Z = true;
      Hit++;
    }
   else
    {
      digitalWrite(LED3, LOW);
    }
  Odor2 = digitalRead(LED2);
  if(button2WasPressed == true && Odor2 == true && Z2 == false)
    {
      Z2 = true;
    }
  if(button2WasPressed != button2WasPressedP && button2WasPressed == true)
    {
      Lick++;
      button2WasPressedP = button2WasPressed;
    }
  else
    {
      button2WasPressedP = button2WasPressed;
    }
}
