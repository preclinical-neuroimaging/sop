  // The training starts with lick teaching. First give a sequence of 20 rewards. Next, only a reward is given after a lick is detected. 
// variable definitions

#include <ezButton.h>

const int LED3 = 6; // green LED, output 3
const int BUTTON_2 = 7; // lick detection
const int NUMBER_OF_REPEAT = 10; // amount of water rewards given

int loopCount = 0; // loop iteration
bool button2WasPressed = false; // when lick is detected
bool X = false; // to ensure that after 20 lick rewards are given, the program continues with lick-teaching

long CurrentTime;

ezButton button(12); // create ezButton object that attach to pin

void setup() {
    Serial.begin(9600); // to see values from Arduino and present it on the computer  
    button.setDebounceTime(50); // set debounce time to 50 milliseconds

    while (!button.isPressed())
      button.loop(); // MUST call the loop() function first
    
    pinMode(LED3, OUTPUT); // set LED3 (green) as an output 3
    pinMode(BUTTON_2, INPUT); // set button 2 as an input 2
    pinMode(13, OUTPUT);
}

void loop () {
  if(X == false)
  {
    for(int i = 1; i <= NUMBER_OF_REPEAT; i++)
      { 
        reward(i);
        if(i == NUMBER_OF_REPEAT)
          {
            X = true;
          }
      }
  }
  button2WasPressed = digitalRead(BUTTON_2);
  lickreward();
}



void reward(int i) { // repeated reward delivery, irrespective of licking behaviour
  
  CurrentTime = millis();
  
  if (loopCount < NUMBER_OF_REPEAT) {
    digitalWrite(LED3, HIGH);
    delay(1000);
    digitalWrite(LED3, LOW);
    delay(1000);
    Serial.print("Start Loop: ");
    Serial.println(i);
    Serial.println(CurrentTime);
  }
}


void lickreward() { // only reward delivery, when a lick is detected

  static int REWARD_LICK_REPEATS = 20;
  CurrentTime = millis();
  
  if (button2WasPressed == true && loopCount < REWARD_LICK_REPEATS) {
    digitalWrite(LED3, HIGH); 
    delay(1000);
    digitalWrite(LED3, LOW);
    delay(1000);
    loopCount++;
    Serial.print("Lick Count: ");
    Serial.println(loopCount);
    Serial.println(CurrentTime);

    }
  }
