// The training continues with odor conditioning, where the reward-associated odor is introduced
// Variable definitions

#include <ezButton.h>

const int LED1 = 2; // valve 1
const int VALVE3 = 4; // valve 3 (depressurize)
const int LED3 = 6; // green LED, output 3
const int BUTTON_2 = 7; // lick detection
const int END_program = 13; // Orange LED is on when one trial is completed
const int NUMBER_OF_REPEAT = 20; // amount of water rewards given

int loopCount = 0; // loop iteration
bool button2WasPressed = false; // when lick is detected
bool X = false; //

ezButton button(12); // create ezButton object that attach to pin

void setup() {
  Serial.begin(9600);
  button.setDebounceTime(50); // set debounce time to 50 milliseconds

  while (!button.isPressed())
    button.loop(); // MUST call the loop() function first

  pinMode(LED1, OUTPUT);
  pinMode(VALVE3, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(BUTTON_2, INPUT);
  pinMode(13, OUTPUT);
}

void loop () {
   if(X == false)
  {
    for(int i = 1; i <= NUMBER_OF_REPEAT; i++)
      {
        reward(i);
        if(i == NUMBER_OF_REPEAT)
          {
            X = true;
          }
      }
  }
  button2WasPressed = digitalRead(BUTTON_2);

}

void reward(int i) { // repeated reward delivery, irrespective of licking behaviour

  if (loopCount < NUMBER_OF_REPEAT) {
    digitalWrite(LED1, HIGH);
    digitalWrite(VALVE3, LOW);
    delay(2000);
    digitalWrite (LED1, LOW);
    digitalWrite(VALVE3, HIGH);
    digitalWrite(LED3, HIGH);
    delay(1000);
    digitalWrite(LED3, LOW);
    delay(1000);
    Serial.print("Start Loop: ");
    Serial.println(i);
  }
}
