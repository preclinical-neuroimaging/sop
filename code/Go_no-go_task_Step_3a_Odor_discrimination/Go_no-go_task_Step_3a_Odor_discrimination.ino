// The training continues with odor discrimination, where the second odor is introduced
// Variable definitions

const int LED1 = 2; // first odor (reward-associated)
const int LED2 = 3; // second odor
const int VALVE3 = 4; // valve 3 (depressurize)
const int LED3 = 6; // green LED, output 3
const int BUTTON_2 = 7; // lick detection
const int END_program = 13; // Orange LED is on when one trial is completed
const int NUMBER_OF_REPEAT = 10; // amount of water rewards given

int loopCount = -1; // loop iteration
bool button2WasPressed = false; // when lick is detected
bool X = false; //

void setup() {
  Serial.begin(9600);

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(VALVE3, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(BUTTON_2, INPUT);
  pinMode(13, OUTPUT);
}

void loop () {
   if(X == false)
  {
    for(int i = 0; i <= NUMBER_OF_REPEAT; i++)
      {
        reward(i);
        if(i == NUMBER_OF_REPEAT)
          {
            X = true;
          }
      }
  }
  button2WasPressed = digitalRead(BUTTON_2);

}

void reward(int i) { // repeated reward delivery, irrespective of licking behaviour

  if (loopCount < NUMBER_OF_REPEAT) {
    digitalWrite(LED2, HIGH);
    digitalWrite(VALVE3, LOW);
    delay(2000);
    digitalWrite (LED2, LOW);
    digitalWrite(LED1, HIGH);
    delay(2000);
    digitalWrite (LED1, LOW);
    digitalWrite(VALVE3, HIGH);
    digitalWrite(LED3, HIGH);
    delay(1000);
    digitalWrite(LED3, LOW);
    delay(1000);
    Serial.print("Start Loop: ");
    Serial.println(i);
  }
}
