SOP for corticosterone measurement by ELISA measurements

1. [Product specification](#product_specification)
2. [Materials](#materials)
3. [Method](#method)


### Product specification

Company: Enzo Life Sciences BVBA
Website: www.enzolifesciences.com
Product name: Corticosterone ELISA Kit
Product code: ADI-900-097

### Materials
#### Materials supplied

1. Donkey anti-Sheep IgG Microtiter Plate, One Plate of 96 Wells, Catalog No. 80-0045
    * A plate using break-apart strips coated with donkey antibody specific to sheep IgG.

2. Corticosterone ELISA Conjugate, 5 mL, Catalog No. 80-0917
    * A blue solution of alkaline phosphatase conjugated with Corticosterone
    
3. Corticosterone ELISA Antibody, 5 mL, Catalog No. 80-0918
    * A yellow solution of a sheep polyclonal antibody to Corticosterone
    
4. Assay Buffer 15 Concentrate, 27 mL, Catalog No. 80-0921
    * Tris buffered saline containing proteins and sodium azide as preservative.

5. Wash Buffer Concentrate, 27 mL, Catalog No. 80-1286
    * Tris buffered saline containing detergents
    
6. Corticosterone Standard, 0.5 mL, Catalog No. 80-0916
    * A solution of 200,000 pg/mL Corticosterone

7. Steroid Displacement Reagent, 2 mL, Catalog No. 80-0925
    * A special formulated displacer to inhibit steroid binding to proteins

8. p-Npp Substrate, 20 mL, Catalog No. 80-0075
    * A solution of p-nitrophenyl phosphate in buffer. Ready to use. 

9. Stop Solution, 5 mL, Catalog No. 80-0247
    * A solution of trisodium phosphate in water. Keep tightly capped. Caution: Caustic. 

10. Corticosterone Assay Layout Sheet, 1 each, Catalog No. 30-0162
11. Plate Sealer, 1 each, Catalog No. 30-0012

Storage: All components of the kit are stable at 4 degrees Celsius until the kit's expiration date. 

#### Other materials needed

1. Deionized or distilled water.
2. Precision pipets for volumes between 5 µL and 1,000 µL.
3. Repeater pipets for dispensing 50 µL and 200 µL. 
4. Disposable beaker for diluting buffer concentrates.
5. Graduated cylinders.
6. A microplate shaker.
7. Adsorbent paper for blotting.
8. Microplate reader capable of reading at 405 nm, preferably with correction between 570 and 590 nm. 

### Methods

Steroid Displacement Reagent (present in the kit) should be added to serum, plasma and other samples containing steroid binding proteins in order to free the bound corticosterone. Such samples should be diluted with 2.5 parts of the reagent for every 97,5 parts of sample.
If working with very small volumes of serum or plasma, there is another protocol. See the protocol below. 

#### ELISA Small Volume Protocol for Serum/Plasma

Corticosterone ELISA Small Volume Protocol for Serum/Plasma
1. Let all solutions come to room temp before use.
2. Aliquot 10 μL of each sample into microfuge tube.
3. Make 1 mL 1:100 Steroid Displacement Reagent (SDR) solution
(immediately before use) using deionized water or PBS (not assay buffer).
4. Add 10 μL 1:100 SDR to each sample tube.
5. Vortex, let stand >5 minutes before diluting with ELISA buffer.
6. Add 380 μL ELISA assay buffer to each plasma tube, vortex. Final dilution
is 1:40. Some samples may require additional dilution so we do recommend that the final dilution with assay buffer should be optimized by each end user for the amount of corticosterone present can vary between sample groups.

#### Protocol to extract hormones from feces

1. Label 16x125mm polypropylene tubes with extraction #’s.

2. Measure 0.5g (+/- 0.05) of mixed, defrosted fecal sample into the corresponding tube, being careful to remove any litter or debris.

3. Record exact weight on fecal extraction sheet, and record any comments (unusually wet or dry, hairball).

4. Fill a labeled 12x75mm polypropylene tube with sample and freeze for future use, if required. Sample labels should include animal ID (name), sample # and date.

5. Add 5 ml of 80% Ethanol in distilled water and cap tubes quickly.

6. Vortex all tubes well, making sure samples are broken up.

7. Place racked tubes in baggies in a horizontal position on a rotator and mix well for 14-18 hours (overnight).

8. Centrifuge tubes for 15 minutes at 1,500rpm.

9. Pipette 1 ml Assay Buffer into labeled 12x75mm polypropylene test tubes. Add 1 ml of supernatant to each corresponding tube, cap tightly and store frozen. Dilution tube labels should include animal ID (name), sample #, date and 1:10.

10. Feces and dilutions are stored in gridded boxes. Box labels should include species, animal ID (name or #) and sex (0.1 or 1.0), feces or 1:10’s, zoo, start and finish sample # and dates. This represents a 10-fold dilution (5-fold in Ethanol, since the original volume is 5 ml, and a further 2-fold into buffer, so that the sample will freeze).


#### Reagent preparation

1. Assay Buffer 15
Just before use, prepare the Assay Buffer 15 by diluting 10 mL of the supplied concentrate with 90 mL of deionized water. Discard unused buffer or add up to 0.09% sodium azide (w/v) for storage.

2. Corticosterone Standard
Allow the 200,000 pg/mL Corticosterone standard solution to warm to room temperature. Label five 12 x 75 mm glass tubes #1 through #5. Pipet 900 μL of standard diluent (Assay Buffer 15 or Tissue Culture Media) into tube #1. Pipet 800 μL of standard diluent (Assay Buffer 15 or Tissue Culture Media) into tubes #2 through #5. Add 100 μL of the 200,000 pg/mL standard to tube #1. Vortex thoroughly. Add 200 μL of tube #1 to tube #2 and vortex thoroughly. Add 200 μL of tube #2 to tube #3 and vortex. Continue this for tubes #4 and #5.
The concentration of Corticosterone in tubes #1 through #5 will be 20,000, 4,000, 800, 160 and 32 pg/mL respectively. See the Corticosterone Assay Layout Sheet for dilution details.
Diluted standards should be used within 60 minutes of preparation. 

3. Wash Buffer
Prepare the Wash Buffer by diluting 5 mL of the supplied concentrate with 95 mL of deionized water. This can be stored at room temperature until the kit expiration date, or for 3 months, whichever is earlier.

#### Assay procedure

Bring all reagents to room temperature for at least 30 minutes prior to opening.
All standards and samples should be run in duplicate.

1. Refer to the Assay Layout Sheet to determine the number of wells to be used and put any remaining wells with the desiccant back into the pouch and seal the ziploc. Store unused wells at 4°C.
2. Pipet 100 μL of standard diluent (Assay Buffer 15 or Tissue Culture Media) into the NSB and the B0 (0 pg/mL Standard) wells.
3. Pipet 100 μL of Standards #1 through #5 into the appropriate wells.
4. Pipet 100 μL of the Samples into the appropriate wells.
5. Pipet 50 μL of Assay Buffer 15 into the NSB wells.
6. Pipet 50 μL of blue Conjugate into each well, except the Total Activity (TA) and Blank wells.
7. Pipet 50 μL of yellow Antibody into each well, except the Blank, TA and NSB wells.
NOTE: Every well used should be Green in color except the NSB wells which should be Blue. The Blank and TA wells are empty at this point and have no color.
8. Incubate the plate at room temperature on a plate shaker for 2 hours at ~500 rpm. The plate may be covered with the plate sealer provided, if so desired.
9. Empty the contents of the wells and wash by adding 400 μL of wash solution to every well. Repeat the wash 2 more times for a total of 3 washes.
10. After the final wash, empty or aspirate the wells, and firmly tap the plate on a lint free paper towel to remove any remaining wash buffer.
11. Add 5 μL of the blue Conjugate to the TA wells.
12. Add 200 μL of the pNpp Substrate solution to every well. Incubate at room temperature for 1 hour without shaking.
13. Add 50 μL of Stop Solution to every well. This stops the reaction and the plate should be read immediately.
14. Blank the plate reader against the Blank wells, read the optical density at 405 nm, preferably with correction between 570 and 590 nm. If the plate reader is not able to be blanked against the Blank wells, manually subtract the mean optical density of the Blank wells from all readings.

#### Calculation of results

1. Calculate the average net Optical Density (OD) bound for each standard and sample by subtracting the average NSB OD from the average OD bound:
𝐴𝑣𝑒𝑟𝑎𝑔𝑒 𝑁𝑒𝑡 𝑂𝐷 = 𝐴𝑣𝑒𝑟𝑎𝑔𝑒 𝐵𝑜𝑢𝑛𝑑 𝑂𝐷 − 𝐴𝑣𝑒𝑟𝑎𝑔𝑒 𝑁𝑆𝐵 𝑂𝐷

2. Calculate the binding of each pair of standard wells as a percentage of the maximum binding wells (Bo), using the following formula:
𝑁𝑒𝑡 𝑂𝐷 𝑃𝑒𝑟𝑐𝑒𝑛𝑡 𝐵𝑜𝑢𝑛𝑑 = 𝑁𝑒𝑡 𝐵0 × 100

3. Plot Percent Bound versus Concentration of Corticosterone for the standards. Approximate a straight line through the points. The concentration of Corticosterone in the unknowns can be determined by interpolation.


