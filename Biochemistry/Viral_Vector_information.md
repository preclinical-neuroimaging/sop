pAAV-hSyn-hM4D(Gi)-mCherry: https://www.addgene.org/50475/
- Stock: AAV9 100uL 2.3 * 10^13 vg/mL
- Aliquoted to 5uL epjes
- Location: -80 in PRIME, bagged

pAAV-hSyn-mCherry: https://www.addgene.org/114472/
- Stock: AAV9 100uL 2.7 * 10^13 vg/mL
- Aliquoted to 5uL epjes
- Location: -80 in PRIME, bagged