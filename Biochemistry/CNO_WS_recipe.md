Stock information: https://www.sigmaaldrich.com/NL/en/product/sigma/c0832 

Stock = 5mg
To create Working solution, dissolve the Stock in 10ml NaCl
Working Solution (WS)= 0.5mg/ml 

For 2019-0023-002: injection of 2mg/kg = 0.05mg / 25g mouse
With the WS -> 0.5mg/ml -> 0.1ml i.p. injection
